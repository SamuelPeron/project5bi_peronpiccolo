//
//  ViewController.swift
//  project5bi_PeronPiccolo_CalcolaIP
//
//  Created by Studente on 22/11/2018.
//  Copyright © 2018 Studente. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textIP: UITextField!
    @IBOutlet weak var labelIP: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func btnCalcolaIP(_ sender: Any) {
    }
    
    private func calcolaIP(_ bin32: String) -> [Int] {
        var arr = [Int]()
        
        for i in stride(from: 0, to: 32, by: 8) {
            let start = bin32.index(bin32.startIndex, offsetBy: i)
            let end = bin32.index(bin32.endIndex, offsetBy: (i - 24))
            let range = start..<end
            arr.append(fromStringBinToInt(String(bin32[range])))
        }
        
        return arr
    }
    private func fromStringBinToInt(_ bin : String) -> Int {
        if let dec = Int(bin, radix : 2) {
            return dec
        } else {
            return -1
        }
    }
}